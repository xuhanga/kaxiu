package com.kaxiu.config.redis;

import com.alibaba.fastjson.JSONObject;
import com.kaxiu.persistent.entity.BasicUser;

/**
 * @author: LiYang
 * @create: 2019-08-12 21:17
 * @Description:
 **/
public interface RedisService {

    /**
     * 保存用户位置信息
     * @return
     */
    boolean setLocation(JSONObject location);

    /**
     * 设置未登陆用户的位置信息，mqtt使用
     * @param userId
     * @param location
     * @return
     */
    boolean setLocation(Long userId, JSONObject location);

    JSONObject getLocation();


    /**
     * 保存验证码，五分钟
     * @param mobile
     * @param code
     * @return
     */
    boolean setMobileCode(String mobile, String code);

    String getMobileCode(String mobile);

    /**
     * set存数据
     * @param key
     * @param value
     * @return
     */
    boolean set(String key, String value);


    /**
     *
     * @param key
     * @param value
     * @param expire seconds
     * @return
     */
    boolean set(String key, String value, Long expire);


    boolean setWxUser(BasicUser user);

    /**
     * 登陆后才可以获取的到
     * @return
     */
    BasicUser getWxUser();

    /**
     * get获取数据
     * @param key
     * @return
     */
    String get(String key);

    /**
     * 设置有效天数
     * @param key
     * @param expire
     * @return
     */
    boolean expire(String key, long expire);

    /**
     * 移除数据
     * @param key
     * @return
     */
    boolean remove(String key);

}
