package com.kaxiu.web.common;


import com.kaxiu.common.base.AbstractBaseController;
//import com.kaxiu.service.IServiceCategoryService;
import com.kaxiu.service.IServiceCategoryService;
import com.kaxiu.vo.ResultDataWrap;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 维修类别(商品)表 前端控制器
 * </p>
 *
 * @author ly
 * @since 2019-08-01
 */
@RestController
@RequestMapping("/kaxiu/service-category")
public class ServiceCategoryController extends AbstractBaseController {

    @Resource
    private IServiceCategoryService categoryService;

    @GetMapping("")
    public ResultDataWrap getCategory(){
        return buildResult(categoryService.getAllCategory());
    }

}

