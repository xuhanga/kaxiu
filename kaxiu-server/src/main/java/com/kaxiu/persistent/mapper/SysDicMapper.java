package com.kaxiu.persistent.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kaxiu.persistent.entity.SysDic;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface SysDicMapper extends BaseMapper<SysDic> {

}
