package com.kaxiu.persistent.entity;

import com.kaxiu.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户角色中间表
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@Accessors(chain = true)
public class SysRoleUser {

private static final long serialVersionUID=1L;

    /**
     * id
     */
    private Long id;

    /**
     * user id
     */
    private String sysUserId;

    /**
     * 角色id
     */
    private String sysRoleId;


}
