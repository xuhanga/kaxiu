package com.kaxiu.persistent.entity;

import java.math.BigDecimal;
import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 资金账户记录表
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class AccountLog extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 账户编号
     */
    private Long accountId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 变动金额
     */
    private BigDecimal amount;

    /**
     * 流向,流入：1，流出：2
     */
    private Integer fundDirection;

    /**
     * 变更业务类型：支付：1，提现：2
     */
    private String bizType;

    /**
     * 变更关联的业务id
     */
    private String bizId;

    /**
     * 支付交易id
     */
    private String transactionId;

}
