package com.kaxiu.service;

import com.kaxiu.persistent.entity.ServiceCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 维修类别(商品)表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IServiceCategoryService extends IService<ServiceCategory> {

    List<ServiceCategory> getAllCategory();

}
