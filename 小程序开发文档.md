

# 小程序开发文档

- 小程序使用uniapp进行开发
- ajax请求使用vmeitime-http
> baseUrl修改在common/vmeitime-http/interface.js 中config下

### 项目运行流程
1. 下载安装配置[HBuilderX](https://download.dcloud.net.cn)
2. 分别倒入用户端与维修端小程序，并运行至小程序即可，前提是要正常运行服务器端项目以及启动的各项服务

