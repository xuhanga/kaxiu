


const getDicByKey = function(key){
	let dic = uni.getStorageSync("dictionary")
	if(dic){
		let val = dic.filter(item => item.dicKey == key)
		if(val && val[0]){
			return val[0].dicValue
		}
	}
}

const getCateById = function(id){
	let catagory = uni.getStorageSync("categorys")
	if(catagory){
		let res = catagory.filter(e => e.id == id)
		return res.length > 0 ? res[0] : undefined
	}
}

export default{
	getDicByKey,
	getCateById
}